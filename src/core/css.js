const utils = {
	minify: (str) =>
		str
			.replace(/[\r\t]/g, " ")
			.replace(/[\n]/g, " ")
			.replace(/^\s+|\s+$|\s+(?=\s)/g, ""),
};

function css(strings, ...values) {
	return (props) => {
		const resolvedValues = values.map((value) => {
			if (typeof value === "function") {
				return value(props);
			}
			return value;
		});

		let result = "";
		for (let i = 0; i < strings.length; i++) {
			result +=
				strings[i] +
				(resolvedValues[i] !== undefined ? resolvedValues[i] : "");
		}
		return result;
	};
}

const parseToJson = (css) => {
	const rules = css.split(/;\n|;|\n/);
	return rules.reduce((acc, rule) => {
		// skip empty entries created by newlines
		if (!rule) return acc;

		const pair = rule.split(":");

		acc[pair[0]] = pair[1];
		return acc;
	}, {});
};

const parseToRGB = (hex, alpha = 1) => {
	const [r, g, b] = hex.match(/\w\w/g).map((x) => parseInt(x, 16));
	return `rgba(${r},${g},${b},${alpha})`;
};

function shadeColor(color, percent) {
	var R = parseInt(color.substring(1, 3), 16);
	var G = parseInt(color.substring(3, 5), 16);
	var B = parseInt(color.substring(5, 7), 16);

	R = parseInt((R * (100 + percent)) / 100);
	G = parseInt((G * (100 + percent)) / 100);
	B = parseInt((B * (100 + percent)) / 100);

	R = R < 255 ? R : 255;
	G = G < 255 ? G : 255;
	B = B < 255 ? B : 255;

	var RR = R.toString(16).length == 1 ? "0" + R.toString(16) : R.toString(16);
	var GG = G.toString(16).length == 1 ? "0" + G.toString(16) : G.toString(16);
	var BB = B.toString(16).length == 1 ? "0" + B.toString(16) : B.toString(16);

	return "#" + RR + GG + BB;
}

export default utils;
export { shadeColor, parseToRGB, parseToJson, css };
