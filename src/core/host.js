export default `
	:host {
		all: initial;
		font: inherit;
		position: relative;
		display: inline-block;
		transition: opacity 200ms linear;
		opacity: 0;
	}

	:host(:defined) {
		opacity: 1;
	}

	:host([hidden]) {
		display: none;
	}

	:host([disabled]) {
		cursor: not-allowed;
		opacity: 0.6;
	}

	:host([hidden]),
	:host([hidden]) *,
	:host([disabled]),
	:host([disabled]) *,
	:host([freeze]),
	:host([freeze]) * {
		pointer-events: none;
	}

	:host *,
	:host *:after,
	:host *:before {
		box-sizing: border-box;
	}
`;
