import { $ } from "./render";

const toKebabCase = (string) => {
	let result = string
		.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, "$1-$2")
		.toLowerCase();
	return result.charAt(0) === "-" ? result.slice(1) : result;
};

const getCssProperty = (prop) =>
	getComputedStyle(document.documentElement).getPropertyValue(`--ui-${prop}`);

const getCSSProps = (props) => {
	let mapProps = {};
	const arrayProps = Array.isArray(props) ? [...props] : [props];
	arrayProps &&
		arrayProps.forEach((prop) => {
			mapProps[toKebabCase(prop)] = getCssProperty(prop);
		});

	return mapProps;
};

const parseStyles = (stylesheet) => {
	const css = Object.entries(stylesheet)
		.map(([prop, value]) => {
			if (typeof value === "object") {
				// Si el valor es un objeto, lo trata como un selector anidado
				return `${toKebabCase(prop)} { ${parseStyles(value)} }`;
			} else {
				return `${toKebabCase(prop)}: ${value};`;
			}
		})
		.join("\n");
	return css;
};

const mapToClassObject = (classes) => {
	return Object.entries(classes)
		.map(([key, value]) => value && key)
		.filter((key) => key);
};

const randomKey = (range) => {
	const chars =
		"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	let result = "";
	for (let i = 0; i < range; i++) {
		const randomIndex = Math.floor(Math.random() * chars.length);
		result += chars[randomIndex];
	}
	return result;
};

export {
	toKebabCase,
	getCssProperty,
	getCSSProps,
	parseStyles,
	mapToClassObject,
	randomKey,
};
