import { component } from "haunted";
import { toKebabCase } from "./utils";

export const connect =
	(element) =>
	(options = { shadow: true }) => {
		const tag = options?.tag || toKebabCase(element.name);
		const props = options?.props || [];
		const shadow = options.shadow;

		Object.assign(element, {
			componentName: tag,
			observedAttributes: props,
		});

		if (typeof window !== "undefined" && !window.customElements.get(tag)) {
			window.customElements.define(
				tag,
				component(element, { useShadowDOM: shadow })
			);
		}
	};
