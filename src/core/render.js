import { Fragment, NamespaceSVG, customAttributes } from "./constants";
import { randomKey, parseStyles, mapToClassObject } from "./utils";
import utils from "./css";
import host from "./host";

const privateAttributes = (key) =>
	!customAttributes.includes(key.toLowerCase());

function h(type, props, ...children) {
	props = props || {};

	if (typeof type === "function") {
		if (type.prototype !== undefined) {
			return new type(props);
		}
		return type(props);
	}

	children = children.flat();
	const mappedChildren = children.map((child) =>
		typeof child === "object"
			? child
			: {
					type: "textNode",
					props: {
						text: child,
					},
			  }
	);

	return {
		type,
		key: props.key || null,
		props: {
			...props,
			children: mappedChildren,
		},
	};
}

const createHTMLElement = ({ type, children = [] }) => {
	const template = children[0]?.props?.text || "";
	const element = document.createElement(type);

	if (template.match(/<("[^"]*"|'[^']*'|[^'">])*>/)) {
		element.innerHTML = template;
	}

	return element;
};

const createStylesheet = (styles, main) => {
	let css = "";
	if (typeof styles === "string") css = styles;
	if (typeof styles === "object") css = `:host { ${parseStyles(styles)} }`;

	return utils.minify(`${main ? host : ""}${css}`);
};

const $ = (node) => {
	let element = null;
	if (node.type === "textNode") {
		const element = document.createTextNode(node.props.text || "");
		return element;
	}

	if (node.type === "template") {
		return createHTMLElement({
			type: "span",
			children: node.props.children,
		});
	}

	if (node.type === "host") {
		element = document.createDocumentFragment();
		const styles = document.createElement("style");
		const hostmain = node.props?.$main || false;
		const css = node.props?.stylesheet || "";

		styles.textContent = createStylesheet(css, hostmain);
		styles.id = `host-${node.props.name || "-"}-${randomKey(8)}`.replace(
			/--/gi,
			"-"
		);

		element.appendChild(styles);
		return mapChilds(node.props.children, element);
	}

	const elementName = node.type.toLowerCase();
	const isSVG = ["svg", "path"].includes(elementName);
	const isFragment = ["fragment"].includes(elementName);
	const isFunction = typeof elementName === "function";

	element = isSVG
		? document.createElementNS(NamespaceSVG, elementName)
		: isFragment
		? document.createDocumentFragment()
		: isFunction
		? elementName(node.props)
		: document.createElement(elementName);

	for (let [prop, propValue] of Object.entries(node.props)) {
		if (privateAttributes(prop)) {
			if (
				prop.startsWith("on") &&
				prop.toLowerCase() in window &&
				typeof propValue === "function"
			) {
				const event = prop.toLowerCase().substring(2);
				element.addEventListener(event, propValue);
			} else if (prop === "style" && typeof propValue === "object") {
				Object.assign(element.style, propValue);
			} else if (prop.startsWith("store-")) {
				const key = prop.toLowerCase().substring(6);
				element.store = {
					...element.store,
					[key]: propValue,
				};
			} else if (prop === "className") {
				const classes = Array.isArray(propValue)
					? propValue
					: typeof propValue === "object"
					? mapToClassObject(propValue)
					: [propValue];

				element.setAttribute("class", classes.join(""));
			} else if (prop === "htmlFor") {
				element.setAttribute("for", propValue);
			} else {
				element.setAttribute(prop, propValue);
			}
		}
	}

	return mapChilds(node.props.children, element);
};

const mapChilds = (childs, element) => {
	if (childs) {
		for (let i = 0, len = childs.length; i < len; i++) {
			const child = $(childs[i]);
			element.appendChild(child);
		}
	}

	return element;
};

export { h as jsx, h as jsxs, h as jsxDEV, $, Fragment };
