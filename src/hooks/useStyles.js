import { useMemo } from "haunted";

/**
 *
 * @param {*} css //function to return string stylesheet
 * @param {*} props // observable props for update stylesheet
 * @returns
 */
const useStyles = (css, props = {}) => {
	const styles = useMemo(() => {
		let stylesheet = "";
		if (css instanceof Function) stylesheet = css(props);
		if (typeof css === "string") stylesheet = css;
		return stylesheet;
	}, [css, props]);

	return styles();
};

export { useStyles };
