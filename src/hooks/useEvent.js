const useEvent = (element, type) => {
	return {
		emit: (data, options = {}) => {
			let eventOptions = {
				bubbles: true,
				composed: true,
				enumerable: true,
				writable: true,
				configurable: true,
			};

			if (options) {
				eventOptions = {
					...eventOptions,
					bubbles: options.bubbles || true,
					composed: options.composed || true,
					enumerable: options.enumerable || true,
					writable: options.writable || true,
					configurable: options.configurable || true,
				};
			}
			const event = new CustomEvent(type, {
				bubbles: eventOptions.bubbles,
				composed: eventOptions.composed,
			});

			Object.defineProperty(event, "data", {
				...eventOptions,
				value: data,
			});

			element.dispatchEvent(event);
		},
	};
};

export default useEvent;
