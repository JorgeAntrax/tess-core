import { MISSING_ICON } from "../core/constants";
import { getCssProperty } from "../core/utils";

const URL_BASE = getCssProperty("src-iconset");
const STORAGE_NAME = getCssProperty("storage-iconset");

const saveIcon = (name, icon) => {
  const $storage = JSON.parse(sessionStorage.getItem(STORAGE_NAME)) || {};

  const updateStorage = {
    ...$storage,
    [name]: icon,
  };

  sessionStorage.setItem(STORAGE_NAME, JSON.stringify(updateStorage));
};

const getIcon = (name, path = null) =>
  new Promise((resolve, reject) => {
    fetch(`${URL_BASE}/${path ? path + "/" : ""}${name}.svg`, {
      method: "GET",
      headers: new Headers({
        "Accept": "*/*",
        "Connection": "Keep-alive",
        "Accept-Language": "en-GB",
        "Accept-Encoding": "gzip, deflate",
        "Content-Type": "text/xml; charset=utf-8",
        "Access-Control-Allow-Origin": "*",
      }),
    })
      .then(icon => icon.text())
      .then(icon => {
        const pattern = new RegExp("(?:<script.+?>.+?</script>)");
        if (!pattern.test(icon)) {
          resolve(icon);
        } else {
          reject(MISSING_ICON);
        }
      })
      .catch(() => reject(MISSING_ICON));
  });

const useIcon = name =>
  new Promise((resolve, reject) => {
    const $storage = sessionStorage.getItem(STORAGE_NAME) || null;

    if (!!$storage) {
      const iconSet = JSON.parse($storage);
      if (name in iconSet) {
        resolve(iconSet[name]);
        rerturn;
      }
    }

    getIcon(name)
      .then(icon => {
        saveIcon(name, icon);
        resolve(icon);
      })
      .catch(err => resolve(err));
  });

export default useIcon